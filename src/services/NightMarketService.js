import { NightMarketData } from "../data/NightMarketData.js";
import { GetRandomFrom, GetRandomInt } from "../modules/RandomModule.js";

const CURRENT_NIGHT_MARKETS = "current-night-markets";

export default class NightMarketService {
  getRandom() {
    return this.getRandomNightMarkerTypes();
  }

  getRandomNightMarkerTypes(n = 2) {
    return GetRandomFrom(n, NightMarketData).map((x) => {
      const shelves = this.getRandomShelves(x.shelves);
      return {
        name: x.name,
        description: x.description,
        shelves: shelves,
      };
    });
  }

  getRandomShelves(shelves, n = 10) {
    return GetRandomFrom(GetRandomInt(n) + 1, shelves);
  }

  load() {
    const s = localStorage.getItem(CURRENT_NIGHT_MARKETS);

    if (s) {
      return JSON.parse(s);
    }

    return null;
  }

  save(nightMarkets) {
    localStorage.setItem(CURRENT_NIGHT_MARKETS, JSON.stringify(nightMarkets));
  }
}
