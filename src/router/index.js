import { createRouter, createWebHashHistory } from "vue-router";
import Home from "@/views/Home.vue";
import Layout from "@/views/Layout.vue";
import NightMarket from "@/views/NightMarket.vue";
import Vendit from "@/views/Vendit.vue";

const routes = [
  {
    path: "/",
    component: Layout,
    children: [
      {
        path: "",
        component: Home,
      },
      {
        path: "night-market",
        component: NightMarket,
      },
      {
        path: "vendit",
        component: Vendit,
      },
    ],
  },
];

export const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
