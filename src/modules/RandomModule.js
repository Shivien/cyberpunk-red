import { GenerateArray, SwapValues } from "./ArrayModule.js";

export function GetRandomInt(max) {
  return Math.floor(Math.random() * max);
}

export function Shuffle(arr) {
  for (let index = arr.length - 1; index > 0; index--) {
    const r = GetRandomInt(index);
    SwapValues(arr, r, index);
  }
  return arr;
}

export function GetRandomFrom(n, arrayFrom) {
  const arrayRandom = Shuffle(GenerateArray(arrayFrom.length));
  const arrayReturn = [];

  for (let index = 0; index < n; index++) {
    const r = arrayRandom.pop();
    arrayReturn.push(arrayFrom[r]);
  }

  return arrayReturn;
}

export function GetOneRandomFrom(arrayFrom) {
  const r = GetRandomInt(arrayFrom.length);

  return arrayFrom[r];
}
