/**
 * Generate array with numbers 0 to length - 1.
 */
export function GenerateArray(length) {
  const arrayReturn = [];

  for (let index = 0; index < length; index++) {
    arrayReturn.push(index);
  }

  return arrayReturn;
}

/**
 * Swap values from two position in array.
 */
export function SwapValues(arr, posA, posB) {
  const tmp = arr[posA];
  arr[posA] = arr[posB];
  arr[posB] = tmp;

  return arr;
}
